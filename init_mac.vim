set nocompatible              " be iMproved, required
filetype off                  " required


"---Base Configs---
set expandtab
set laststatus=2
set list
set mouse=a
set noshowmode
set number relativenumber
set tabstop=2
set shiftwidth=2
set showmatch
set termguicolors

"auto reload
set autoread

syntax on

"Clearing out gutter for ALE
highlight clear SignColumn

"--Plug Ins---
call plug#begin('~/.config/nvim/plugged')
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'itchyny/lightline.vim'
Plug 'Quramy/vim-js-pretty-template'
Plug 'ervandew/supertab'
"NERDTree
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'scrooloose/nerdtree'
"Fuzzy Finder
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
"Code Completion
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
" Stuff to try to make JS/TS/TSX work
Plug 'sheerun/vim-polyglot'
Plug 'dense-analysis/ale'
"Better Nav in Nvim/TMUX
"Plug 'christoomey/vim-tmux-navigator'
"Comment out code with CMD + /
Plug 'scrooloose/nerdcommenter'
"Pretty Icons
Plug 'ryanoasis/vim-devicons'
"GROOOOVY
Plug 'gruvbox-community/gruvbox'
Plug 'sainnhe/gruvbox-material'
"OH DIP MARKDOWN PREVIEW
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }

call plug#end()
let g:gruvbox_contrast_dark = 'hard'
if exists('+termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
let g:gruvbox_invert_selection='1'

colorscheme gruvbox
set background=dark

"-- Command for NERDCommenter --
vmap ++ <plug>NERDCommenterToggle
nmap ++ <plug>NERDCommenterToggle

"--- NERDTree ---
" How can I close vim if the only window left open is a NERDTree?
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__', 'node_modules']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite

"---ALE Configs---
"Fix files with prettier and then ESLint
let g:ale_fixers = {
\   'javascript': ['prettier', 'eslint'],
\   'typescript': ['prettier', 'eslint'],
\}
let g:ale_fix_on_save = 1
"Remap moving between errors with CTRL-K/J
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
"Setting warning icons
let g:ale_sign_error = '🔥 '
let g:ale_sign_warning = '⚡️'
highlight clear ALEErrorSign
highlight clear ALEWarningSign

"---Setting up LightLine---
let g:lightline = {
      \ 'colorscheme': 'seoul256',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'filename' ]]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead',
      \   'fugitive': 'LightlineFugitive',
      \   'readonly': 'LightlineReadonly',
      \   'modified': 'LightlineModified',
      \   'filename': 'LightlineFilename'
      \ },
      \ 'separator': { 'left': '', 'right': '' },
      \ 'subseparator': { 'left': '', 'right': '' }
      \ }
function! LightlineModified()
  if &filetype == "help"
    return ""
  elseif &modified
    return "+"
  elseif &modifiable
    return ""
  else
    return ""
  endif
endfunction

function! LightlineReadonly()
  if &filetype == "help"
    return ""
  elseif &readonly
    return ""
  else
    return ""
  endif
endfunction

function! LightlineFugitive()
  if exists("*fugitive#head")
    let branch = fugitive#head()
    return branch !=# '' ? "\ue0a0 ".branch : ''
  endif
  return ''
endfunction

function! LightlineFilename()
  return ('' != LightlineReadonly() ? LightlineReadonly() . ' ' : '') .
       \ ('' != expand('%:t') ? expand('%:t') : '[No Name]') .
       \ ('' != LightlineModified() ? ' ' . LightlineModified() : '')
endfunction

" #SUPERTAB {{{
let g:SuperTabDefaultCompletionType = "<c-n>"
let g:SuperTabClosePreviewOnPopupClose = 1
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<C-x><C-o>"
" }}}

"FZF stuff
map <C-p> :Files<CR>
let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8 } }
let $FZF_DEFAULT_OPTS='--reverse'

