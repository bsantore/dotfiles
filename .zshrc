# Adds `~/.scripts` and all subdirectories to $PATH
#export PATH="$PATH:$(du "$HOME/.scripts/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
export PATH="/home/dp/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

autoload -U compinit && compinit
autoload -U colors && colors

#GIT
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn
precmd() {
    vcs_info
}
setopt prompt_subst

#PROMPT='%/~ '
prompt_git='${vcs_info_msg_0_}'
prompt_host='%F{$vi_mode_color}%m%f'
prompt_user='%F{green}%n%f'
prompt_separator='%F{black}.%f'
prompt_exit_status='%(?..%K{yellow}%F{red}%?%k%f  )'
prompt_current_dir='%F{cyan}%~%f'
promt_git_branch="\$(git_branch)"
PROMPT="$prompt_user  "

# precmd() {
#     # As always first run the system so everything is setup correctly.
#     vcs_info
#     # And then just set PS1, RPS1 and whatever you want to. This $PS1
#     # is (as with the other examples above too) just an example of a very
#     # basic single-line prompt. See "man zshmisc" for details on how to
#     # make this less readable. :-)
#     if [[ -n ${vcs_info_msg_0_} ]]; then
#         # Oh hey, nothing from vcs_info, so we got more space.
#         # Let's print a longer part of $PWD...
# 		PROMPT="$prompt_user: $prompt_git "
#     else
#         # vcs_info found something, that needs space. So a shorter $PWD
#         # makes sense.
# 		PROMPT="$prompt_user  "
#     fi
# }

RPROMPT="$prompt_current_dir"

#AUTO COMPLETE NO CASE
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
setopt nocasematch

#ALIAS
alias ls='ls -a --color=auto'
alias n='nvim'
alias l='ls -lah'
alias L='ls -a'
alias digi='ssh danp@45.55.53.151'
alias nvim-update="nvim -c 'PlugUpgrade | PlugUpdate | PlugClean | silent UpdateRemotePlugins' -c 'qa'"
alias brup='brew update'
alias bpy='bpython'
#alias http='python -m http.server'
alias cloudup="python /home/dp/Downloads/google-cloud-sdk/bin/dev_appserver.py ."
alias cloudup-clear="python /home/dp/Downloads/google-cloud-sdk/bin/dev_appserver.py --clear_datastore=yes ."
alias kbra="sudo openconnect -u dpiston https://secure.krollbondratings.com/kbra-vpn-nocert"
alias fit="rm -rf node_modules"

up() {
	for i in {1..$1};
	do
		cd ..
	done
}



export EDITOR=/usr/bin/nvim
