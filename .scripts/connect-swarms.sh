#!/bin/bash

RG=$1
SUB=$2
MAN=$3

#Grab ip of 1st swam manager vm
VMIP=$(az network nic list -g $RG --subscription $SUB | jq "[.[].ipConfigurations[] | select(.id | test(\"Swarm\"))] | .[$MAN].privateIpAddress" -r)
echo "IP = $VMIP"

#reset keygen
echo "Resetting ssh keys"
ssh-keygen -R $VMIP

#Import new key for ip
echo "Adding ip to known_hosts"
ssh-keyscan -H $VMIP >> ~/.ssh/known_hosts

#Set pass
export SSHPASS=$apw

sshpass -e ssh azureuser@$VMIP

