let g:python_host_prog = '/home/dp/.pyenv/versions/neovim2/bin/python'
let g:python3_host_prog = '/home/dp/.pyenv/versions/neovim3/bin/python'

set nocompatible              " be iMproved, required
filetype off                  " required


set noshowmode
set expandtab
set tabstop=4
set shiftwidth=4
call plug#begin('~/.config/nvim/plugged')
Plug 'tpope/vim-fugitive'
Plug 'git://git.wincent.com/command-t.git'
Plug 'itchyny/lightline.vim'
Plug 'scrooloose/nerdtree'
Plug 'martinda/Jenkinsfile-vim-syntax'
"Plug 'Shougo/deoplete.nvim'
"Plug 'zchee/deoplete-jedi'
"Plug 'pangloss/vim-javascript'
Plug 'flazz/vim-colorschemes'
Plug 'leafgarland/typescript-vim'
call plug#end()

"Setting up LightLine
let g:lightline = {
      \ 'colorscheme': 'seoul256',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'fugitive', 'filename' ]]
      \ },
      \ 'component_function': {
      \   'fugitive': 'LightlineFugitive',
      \   'readonly': 'LightlineReadonly',
      \   'modified': 'LightlineModified',
      \   'filename': 'LightlineFilename'
      \ },
      \ 'separator': { 'left': '', 'right': '' },
      \ 'subseparator': { 'left': '', 'right': '' }
      \ }

function! LightlineModified()
  if &filetype == "help"
    return ""
  elseif &modified
    return "+"
  elseif &modifiable
    return ""
  else
    return ""
  endif
endfunction

function! LightlineReadonly()
  if &filetype == "help"
    return ""
  elseif &readonly
    return ""
  else
    return ""
  endif
endfunction

function! LightlineFugitive()
  if exists("*fugitive#head")
    let branch = fugitive#head()
    return branch !=# '' ? "\ue0a0 ".branch : ''
  endif
  return ''
endfunction

function! LightlineFilename()
  return ('' != LightlineReadonly() ? LightlineReadonly() . ' ' : '') .
       \ ('' != expand('%:t') ? expand('%:t') : '[No Name]') .
       \ ('' != LightlineModified() ? ' ' . LightlineModified() : '')
endfunction

set laststatus=2


"NerdTree
let NERDTreeMapActivateNode='<right>'
let NERDTreeShowHidden=1
map <C-n> :NERDTreeToggle<CR>


filetype on
syntax on

set number
"Setting relative numbers
if(&relativenumber == 1)
    set number
  else
    set relativenumber
  endif
set showmatch

let g:deoplete#enable_at_startup = 1


